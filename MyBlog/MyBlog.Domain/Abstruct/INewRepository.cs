﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyBlog.Domain.Entities;
namespace MyBlog.Domain.Abstruct
{
    //============================================ИНТЕРФЕЙС ДЛЯ РЕАЛИЗАЦИИ ХРАНИЛИЩА ДАННЫХ============================
    /// <summary>
    /// Интерфейс для реализации хранилища данных(новостей) и операций с базой данных(новостей)
    /// </summary>
    public interface INewRepository
    {

         IEnumerable<New> NewsPost { get;}      //хранилища данных новостей
         New Details(int? NewID);               //операция описание выбранной новости
        void SaveNew(New product);              //сохранение выбранной новости
        void DeleteNew(int? NewID);             //удаление выбранной новости
        void AddNews(New pstNws);               //добавление новой новости
    }

    //=================================================================================================================
}
