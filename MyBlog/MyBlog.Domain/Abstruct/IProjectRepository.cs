﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MyBlog.Domain.Entities;
namespace MyBlog.Domain.Abstruct
{

    /// <summary>
    /// Интерфейс для реализации хранилища данных(проектов) и операций с базой данных(проектов)
    /// </summary>
   public interface IProjectRepository
    {
        
        IEnumerable<Project> ProjectPost { get;}    //хранилище всех проектов
        Project DetailsProject(int? projectID);     //Метод для описание кк проекту
        void SaveProject(Project postproject);      //Метод сохранения изменений в БД
        Project DeleteProject(int? projectID);      //метод удаления элементов в БД
        void AddProject(Project pstProject);        //метод добавления новых элементов в БД


    }
}
