﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.Domain.Concreate;

namespace MyBlog.Domain.Concreate
{
   public class EmailSettings
    {
        public string MailToAddress { get; set; }
        public string MailFromAddress = "dante.l.levi93@gmail.com";
        public bool UseSsl = true;
        public string Username = "dante.l.levi93@gmail.com";
        public string Password = "481516234250ak32";
        public string ServerName = "smtp.gmail.com";
        public int ServerPort = 587;
        public bool WriteAsFile = true;
        public string FileLocation = @"c:\game_store_emails";
    }


    public class EmailMassegeProccessor : IMessageProccesor
    {
        private EmailSettings emailSettings;

        public EmailMassegeProccessor(EmailSettings settings)
        {
            emailSettings = settings;
        }
        public void ProccessMassage(MessageDetails message)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = emailSettings.UseSsl;
                smtpClient.Host = emailSettings.ServerName;
                smtpClient.Port = emailSettings.ServerPort;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(emailSettings.Username,emailSettings.Password);

            }
            
        }
    }
}
