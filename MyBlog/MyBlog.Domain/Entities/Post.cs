﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MyBlog.Domain.Entities
{

    /// <summary>
    /// Реализация сущности базы данных-модель Статьи
    /// </summary>
   public class Post
    {
        public int PostID { get; set; }                         //поле-первичный ключ индификатор базы данных
        [Display(Name ="Название статьи:")]
        [Required(ErrorMessage ="Введите название статьи!!")]
        public string NamePost { get; set; }                    //поле-название статьи
        [Display(Name ="Описание к статье:")]
        [Required(ErrorMessage ="Введите описание к статье!!!")]
        public string descriptionPost { get; set; }             //поле- описание к статье
        [Display(Name ="Автор:")]
        [Required(ErrorMessage ="Введите имя автора статьи!!")]
        public string Author { get; set; }                      //поле-автор статьи
        [Display(Name ="Текст статьи:")]
        [Required(ErrorMessage ="Введите текст статьи!!!")]

        [AllowHtml]
        public string TextPost { get; set; }                    //поле-основное наполнение статьи
        public int countView { get; set; }                      //поле-количество просмотров

        [Display(Name ="Название картинки:")]
        [Required(ErrorMessage ="Введите название изображения!!!")]
        public string NameImage { get; set; }                   //поле-имя изображения

        public string mimeType { get; set; }                    //поле -тип

        public byte[] ImageMainPost { get; set; }               //поле-массив байтов изображения
        [Display(Name = "Время публикации:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage ="Введите дату публикации!!!")]
        public DateTime DatePublic { get; set; }                //поле дата публикации статьи


        public int? CategoryID { get; set; }                    //внешний ключ на относящуюся категорию(отношения один-ко-многим)
        public Category category { get; set; }


    }
}
