﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MyBlog.Domain.Entities
{
    /// <summary>
    /// Реализация сущности базы данных категорий относящиеся к статьям
    /// </summary>
   public class Category
    {
        public int CategoryID { get; set; }     //первичный ключ индификации
        [Display(Name ="описание категории:")]
        [Required(ErrorMessage ="Введите описание для категории!!")]
        public string description { get; set; } //описание к статье
        [Display(Name ="Название категории:")]
        [Required(ErrorMessage ="Введите название категории!!")]
        public string NameCategory { get; set; }    //название категорий

        public int countPost { get; set; }          //количество статей
        public ICollection<Post> posts { get; set; }    //список статей в заданной категории

        public Category()
        {
            posts = new List<Post>();
        }

        [Display(Name ="Название изображения:")]
        [Required(ErrorMessage ="Введите имя изображения!!!")]
        public string NameImageCat { get; set; }        //название картинки

        public string mimeType { get; set; }            //тип картинки

        public byte[] ImageMainCat { get; set; }        //массив байтов изображения
    }
}
