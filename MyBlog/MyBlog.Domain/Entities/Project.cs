﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyBlog.Domain.Abstruct;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MyBlog.Domain.Entities
{
    /// <summary>
    /// реализация сущности базы данных-модель проекты
    /// </summary>
    public class Project
    {
        public int projectID { get; set; }      //первичный ключ индификатор 

        [Display(Name = "Название проекта:")]
        [Required(ErrorMessage ="Введите название проекта!!!")]
        public string Name { get; set; }        //поле-название проекта
        [Display(Name = "Описание:")]
        [Required(ErrorMessage ="Введите описание к проекту!!!")]
        public string description { get; set; }     //поле-описание проекта
        [Display(Name = "Основной текст:")]
        [Required(ErrorMessage ="Введите основной текст проекта!!!")]
        [AllowHtml]
        public string TextProject { get; set; }     //поле основное наполнение статьи проекта
        [Display(Name = "Автор:")]
        [Required(ErrorMessage ="Введите автора проекта!!")]
        public string Author { get; set; }          //поле-автор проекта

        [Display(Name = "Время публикации:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage ="Введите дату публикации проекта!!")]
        public DateTime TimePublic { get; set; }        //пле-время публикации проекта

        [Display(Name = "Название картинки:")]
        [Required(ErrorMessage ="Введите название изображения")]
        public string NameImgProject { get; set; }      //поле-имя изображения

        public string mimeType { get; set; }            //поле -тип изображения

        public byte[] ImageMainProject { get; set; }        //поле-массив байт изображения
    }
}
