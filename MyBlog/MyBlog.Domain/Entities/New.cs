﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MyBlog.Domain.Entities
{

    //=====================================МОДЕЛЬ БАЗЫ ДАННЫХ-ТАБЛИЦА НОВОСТЕЙ======================================
    /// <summary>
    /// класс реализующий модель базы данных-Новостей
    /// </summary>
    public  class New
    {
        public int NewID { get; set; }      //первичный ключ индификации

        [Display(Name="Название новости")]
        [Required(ErrorMessage ="Введите название новости")]
        public string Name { get; set; }    //название новости

        [Display(Name = "Автор новости")]
        [Required(ErrorMessage = "Введите автора новости")]
        public string author { get; set; }      //автор новости

        [Display(Name = "Описание ")]
        [Required(ErrorMessage = "Введите описание к новости")]
        public string Description { get; set; }     //описание новостей

        [Display(Name = "Основной текст новости")]
        [Required(ErrorMessage = "Введите текст")]
        [AllowHtml]
        public string TextNews { get; set; }       //основной текст в новостях
        [Display(Name = "количество просмотров")]
        
        public int CountView { get; set; }          //количество просмотров
        [Display(Name = "Время создания")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime TimeNewAdd { get; set; }      //время добавления

        [Display(Name = "Название картинки")]
        public string NameImg { get; set; }             //название картинки

        public string mimeType { get; set; }            //тип

        public byte[] ImageMain { get; set; }           //массив байтов изображения


    }
}
