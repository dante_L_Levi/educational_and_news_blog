﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MyBlog.Domain.Entities
{
   public class MessageDetails
    {
        [Required(ErrorMessage ="Ошибка!!Введите ваше имя!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Ошибка!!Введите вашу фамилию!")]
        public string SerName { get; set; }
        [Required(ErrorMessage = "Введите текст сообщения!!!")]
        public string TitleMeassage { get; set; }
        [Required]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Неверный формат электронной почты")]
        public string EmailUser { get; set; }
        public string EmailFROM { get; set; }
        [Required]
        [StringLength(800)]
        [Display(Name = "Сообщение")]
        [DataType(DataType.MultilineText)]
        public string TextMassege { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Time { get; set; }

    }
}
