﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBlog.WebUI.Models;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Infrastructure;
using System.Data.Entity;
using PagedList;
using System.IO;
using System.Data.Entity.Infrastructure;
using System.Net;

namespace MyBlog.WebUI.Controllers
{
    /// <summary>
    /// Контроллер отвечающий за работу со статьями
    /// </summary>
    public class PostController : Controller
    {
        AppIdentityDbContext dbpost;

        public PostController()
        {
            dbpost = new AppIdentityDbContext();
        }

        #region Вывод отдельной статьи с текстом
        //============================================================================================================
        //==================================================<основная страница статьи>=========================================================
        [ValidateInput(false)]
        public ActionResult Postdesc(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            else
            {
                Post post = dbpost.posts.Find(id);
                return View(post);
            }

        }

        //===========================================================================================================
        #endregion


        #region Список категорий со статьями
        // ====================================<Вывод списка категорий>===============================================
        /// <summary>
        /// Метод вывода списка категорий
        /// </summary>
        /// <returns></returns>
        public ActionResult ListCategory()
        {
            var catpost = dbpost.categoryPosts;
            return View(catpost.ToList());
        }
        //============================================================================================================
        #endregion

        #region Статьи из выбранной категории
        //========================================<Выводим статьи по выбранной категории c пагинацией>=============================
        /// <summary>
        /// Метод просмотра статей по выбранной категории
        /// </summary>
        /// <param name="id"> Индификатор выбранной категории</param>
        /// <param name="page">Переменная для постраничного вывода</param>
        /// <returns></returns>
        public ActionResult ViewCatSelect(int? id,int? page)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            else
            {
                var catSel = dbpost.categoryPosts.Include(c=>c.posts).FirstOrDefault(c=>c.CategoryID==id);
                ViewBag.CatID =id;
                int pageSize = 6;
                int pageNumber = (page ?? 1);

                return View(catSel.posts.ToPagedList(pageNumber,pageSize));
            }
        }


        #endregion

        #region Панель редактирования Категорий
        //======================================<Панель редактирования статей>=======================================
        /// <summary>
        /// Метод управления Статей
        /// </summary>
        /// <param name="sortOrder">Выбор сортировки</param>
        /// <param name="searchString">Поисковой запрос</param>
        /// <param name="currentFilter">Выбарнный фильтр</param>
        /// <param name="page">Параметр для постраничного вывода</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult EditPannelPost(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            //если мы получаем пустую строку то динамический элемент  ViewBag.NameSortParm=name_des
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            //если sortOrder равен Date ViewBag.DateSortParm=date_desc иначе ViewBag.DateSortParm=Date
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            //------------------------Пагинация--------------------------
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //----------------------------------------------
            var post = from n in dbpost.posts.Include(c=>c.category)
                     select n;
            //если строка поиска не равна  нулю то производим выборку из базы данных по введенным ключам
            if (!String.IsNullOrEmpty(searchString))
            {
                post = post.Where(n => n.NamePost.Contains(searchString) || n.Author.Contains(searchString));


            }

            switch (sortOrder)
            {
                case "name_desc":                       //если sortOrder==name_desc сортировка по имени в порядке убывания
                    post = post.OrderByDescending(n => n.NamePost);
                    break;

                case "Date":                            //если sortOrder==Date сортировка по дате по возрастанию
                    post = post.OrderBy(n => n.DatePublic);
                    break;

                case "date_desc":                       //если sortOrder==date_desc сортировка по дате по убыванию
                    post = post.OrderByDescending(n => n.DatePublic);
                    break;

                default:                                //по умолчанию по имени по возрастанию
                    post = post.OrderBy(n => n.NamePost);
                    break;


            }

            //============================================ПАГИНАЦИЯ====================================================
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(post.ToPagedList(pageNumber, pageSize));
            //=========================================================================================================

        }
        #endregion

        #region Панель редактирования категорий
        //===================================================<Панель редактирования категорий к статьям>========================================================

        //======================================<Панель редактирования категорий>=======================================
        /// <summary>
        /// Метод управления категориями
        /// </summary>
        /// <param name="sortOrder">выбор сортировки</param>
        /// <param name="searchString">Поисковой запрос</param>
        /// <param name="currentFilter">Выбранный фильтр</param>
        /// <param name="page">Параметр для постраничного вывода</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult EditPannelCategory(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            //если мы получаем пустую строку то динамический элемент  ViewBag.NameSortParm=name_des
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.IDSortParm = sortOrder == "ID" ? "ID_desc" : "ID";

            //------------------------Пагинация--------------------------
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //----------------------------------------------
            var cat = from c in dbpost.categoryPosts.Include(p=>p.posts)
                       select c;
            //если строка поиска не равна  нулю то производим выборку из базы данных по введенным ключам
            if (!String.IsNullOrEmpty(searchString))
            {
                cat = cat.Where(c => c.NameCategory.Contains(searchString));


            }

            switch (sortOrder)
            {
                case "name_desc":                       //если sortOrder==name_desc сортировка по имени в порядке убывания
                    cat = cat.OrderByDescending(n => n.NameCategory);
                    break;
                case "ID":
                    cat = cat.OrderBy(n => n.CategoryID);
                    break;
                case "ID_desc":
                    cat = cat.OrderByDescending(n => n.CategoryID);
                    break;



                default:                                //по умолчанию по имени по возрастанию
                    cat = cat.OrderBy(n => n.NameCategory);
                    break;


            }

            //============================================ПАГИНАЦИЯ====================================================
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(cat.ToPagedList(pageNumber, pageSize));
            

        }

        //===========================================================================================================

        #endregion
        //++++++++++++++++++++++++++++++++++++++<CRUD-операции>++++++++++++++++++++++++++++++++++++++++++++++++++++++

        #region Создание новой статьи
        //==================================<Создание новой статьи>==============================================
        /// <summary>
        /// Get-метод создания нового объекта статей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Administrators")]
        public ViewResult CreatePost()
        {
            PopulateCategoryDropDownList();
            return View();
        }
        /// <summary>
        /// POST-метод обработки запроса со страницы CreatePost
        /// </summary>
        /// <param name="npost">Новый объект статей</param>
        /// <param name="uploadImage">Объект отвечающий за доступ к загруженным файлам(изображениям)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult CreatePost(Post npost, HttpPostedFileBase uploadImage)
        {
            try
            {
                if (ModelState.IsValid && uploadImage != null)
                {
                    byte[] DataImg = null;
                    DataImg = ConvertImage(uploadImage);
                    npost.ImageMainPost = DataImg;
                    npost.NameImage = SendNameImage(uploadImage);
                    PopulateCategoryDropDownList();
                    dbpost.posts.Add(npost);
                    dbpost.SaveChanges();
                    TempData["message"] = string.Format("{0}---Объект сохранен в базу данных", npost.NamePost);
                    return RedirectToAction("EditPannelPost");

                }
            }
            catch(RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            PopulateCategoryDropDownList(npost.CategoryID);
            return View(npost);



        }
        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        #endregion

        #region Удаление статей
        //=========================================<Удаление статьи>============================================

        /// <summary>
        /// Get-метод удаление статей
        /// </summary>
        /// <param name="id">Индификатор объекта, который будет удален</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult Delete(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            else
            {
                string NameMes;
                Post el = dbpost.posts.Find(id);
                NameMes = el.NamePost;
                dbpost.posts.Remove(el);
                dbpost.SaveChanges();
                TempData["message"] = string.Format("{0}---Объект удален в базу данных", NameMes);
                return RedirectToAction("EditPannelPost");
            }
           
        }

        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

        #endregion


        #region Редактирование статей
        //==============================================<Редактирование Статей>======================================
        /// <summary>
        /// Get-метод отображения страницы редактирования статей
        /// </summary>
        /// <param name="id">Индификатор объекта, который необходимо редактировать</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
            public ActionResult EditPost(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            else
            {
                Post post = dbpost.posts.Find(id);
                if (post != null)
                {
                    PopulateCategoryDropDownList(post.CategoryID);
                    
                }
                
                return View(post);
            }
           

        }

        /// <summary>
        /// POST-запрос со страницы редактирования для обновления БД
        /// </summary>
        /// <param name="pst">Обнавленный объект , который будет заносится в базу данных</param>
        /// <param name="image">Объект, отвечающий за добавления/загрузки новый файлов(изображений)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult EditPost(Post pst, HttpPostedFileBase image)
        {
           if(pst==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
                if (image != null)
                {
                    dbpost.Entry(pst).State = EntityState.Modified;
                     pst.mimeType = image.ContentType;
                    pst.ImageMainPost = new byte[image.ContentLength];
                    image.InputStream.Read(pst.ImageMainPost, 0, image.ContentLength);
                    

                    dbpost.SaveChanges();
                //-------------------------------------
                TempData["message"] = string.Format("{0}-Объект сохранен в базу данных", pst.NamePost);
                
                return RedirectToAction("AdminNewspost");

            }


            PopulateCategoryDropDownList(pst.CategoryID);
            return View(pst);          //ошибка
           
        }
        #endregion


        #region Описание Статьи
        //===========================================<Описание выбранной статьи>=====================================
        /// <summary>
        /// Дополнительный метод просмотра выбранной статьи(доступен только администратору)
        /// </summary>
        /// <param name="id">Индификатор выбранной статьи</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult DetailPost(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            else
            {
                Post post = dbpost.posts.Find(id);
                return View(post);
            }
           
        }

        #endregion

        #region Дополнительные методы
        //---------------------------------<Подгружаем данные из таблицы Category в comboBox>----------------\
        /// <summary>
        /// Метод отвечающий за подгрузку данных в ComboBox на странице редактирования и создания статей
        /// </summary>
        /// <param name="selectedCategory">Переменная отвечающая за выбранную категорию</param>
        private void PopulateCategoryDropDownList(object selectedCategory = null)
        {
            var CategoryQuery = from cat in dbpost.categoryPosts
                                   orderby cat.NameCategory
                                   select cat;
            ViewBag.CategoryID = new SelectList(CategoryQuery, "CategoryID", "NameCategory", selectedCategory);
        }
        //===========================================================================================================



        //==========================================<Работа с изображениями>==============================================
        /// <summary>
        /// Метод отвечающий за преобразование изображения в массив байтов
        /// </summary>
        /// <param name="uploadImage"></param>
        /// <returns>массив байтов</returns>
        private byte[] ConvertImage(HttpPostedFileBase uploadImage)
        {
            byte[] imageData = null;
            //считываем переданный файл в массив байтов
            using (var binaryReader = new BinaryReader(uploadImage.InputStream))
            {
                imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
            }
            return imageData;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /// <summary>
        /// Метод отвечающий за имя изображения
        /// </summary>
        /// <param name="uploadImage"></param>
        /// <returns></returns>
        private string SendNameImage(HttpPostedFileBase uploadImage)
        {
            string NameImage = uploadImage.FileName; ;
            return NameImage;
        }


        //------------------------------------------------------------
        //===========================================================================================================
        #endregion
        //==========================================<CRUD-операции по категориям>====================================
        #region Детальное описание категории со статьями
        //===========================================<Описание выбранной статьи>=====================================
        /// <summary>
        /// Метод описание категории со статьями
        /// </summary>
        /// <param name="id">Индификатор выбранной категории</param>
        /// <returns>Представление DetailCategory.cshtml</returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
        public ActionResult DetailCategory(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            else
            {
                Category cat = dbpost.categoryPosts.Include(c=>c.posts).FirstOrDefault(c=>c.CategoryID==id);
                
                return View(cat);

            }
        }
        //==========================================================================================================
        #endregion

        #region Удаление категории
        //===========================================<Удаление выбранной категории>==================================
        /// <summary>
        /// Метод удаление выбранной категории
        /// </summary>
        /// <param name="id">Индификатор выбранной категории</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult deleteCat(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            else
            {
                Category cat = dbpost.categoryPosts.Find(id);
                dbpost.categoryPosts.Remove(cat);
                dbpost.SaveChanges();
                return RedirectToAction("EditPannelCategory");
            }
        }
        //=========================================================================================================
        #endregion

        #region Создание категории
        //===========================================<создание категории категории>==================================
        /// <summary>
        /// GET-метод отображения страницы с созданием категории
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
        public ViewResult CreateCategory()
        {
            ViewBag.Posts = dbpost.posts.ToList();
            return View();
        }

        /// <summary>
        /// POST-метод обработки запроса со страницы создания категории
        /// </summary>
        /// <param name="ncat">Новый объект категории, который будет сохраняться в БД</param>
        /// <param name="uploadImage">Объект, отвечащий за обработку файлов(изображений)</param>
        /// <param name="selectedPosts">объект отвечающий за выбранный список  статей в категории</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(Category ncat, HttpPostedFileBase uploadImage, int[] selectedPosts)
        {
            try
            {
                if (ModelState.IsValid && uploadImage != null)
                {
                    byte[] DataImg = null;
                    DataImg = ConvertImage(uploadImage);
                    ncat.ImageMainCat = DataImg;
                    ncat.NameImageCat = SendNameImage(uploadImage);
                    ncat.posts.Clear();
                    if(selectedPosts!=null)
                    {
                        foreach (var p in dbpost.posts.Where(po => selectedPosts.Contains(po.PostID)))
                        {
                            ncat.posts.Add(p);
                        }
                    }
                    dbpost.Entry(ncat).State = EntityState.Added;
                    dbpost.SaveChanges();
                    TempData["message"] = string.Format("{0}---Объект сохранен в базу данных", ncat.NameCategory);
                    return RedirectToAction("EditPannelCategory");

                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            
            return View(ncat);
        }
        //============================================================================================================

        #endregion

        #region Редактирование категории
        //=========================================<Редактирование категорий>=========================================
        /// <summary>
        /// GET-запрос на редактирования выбранного объекта категорий
        /// </summary>
        /// <param name="id">Индификатор ,выбранной категории</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditCategory(int? id)
        {
            Category cat = dbpost.categoryPosts.Include(p=>p.posts).FirstOrDefault(p=>p.CategoryID==id);
            if(cat==null)
            {
                return HttpNotFound();
            }
            ViewBag.Posts = dbpost.posts.ToList();
            return View(cat);

        }

        /// <summary>
        /// POST-метод обработки данных со страницы редактирования категорий
        /// </summary>
        /// <param name="ncat">Редактируемый объект категорий, который сохраняется в БД</param>
        /// <param name="selectedPosts">Выбранный список статей, которые будут включаться в категорию</param>
        /// <param name="image">Объект отвечающий за обработку файлов(изображений)</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpPost]
        public ActionResult EditCategory(Category ncat, int[] selectedPosts, HttpPostedFileBase image=null )
        {
            if (ncat == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (image != null)
            {
                ncat.mimeType = image.ContentType;
                ncat.ImageMainCat = new byte[image.ContentLength];
                image.InputStream.Read(ncat.ImageMainCat, 0, image.ContentLength);
                Category cs = dbpost.categoryPosts.Include(c=>c.posts).FirstOrDefault(c=>c.CategoryID==ncat.CategoryID);
                //------------------------------------

                cs.NameCategory = ncat.NameCategory;
                
                cs.description = ncat.description;
               
                cs.countPost = ncat.countPost;
                cs.NameImageCat = ncat.NameImageCat;
                cs.mimeType = ncat.mimeType;
                cs.ImageMainCat = ncat.ImageMainCat;
                cs.posts.Clear();

                if (selectedPosts != null)
                {
                    //получаем выбранные курсы
                    foreach (var p in dbpost.posts.Where(co => selectedPosts.Contains(co.PostID)))
                    {
                        cs.posts.Add(p);
                    }
                }
                dbpost.Entry(cs).State = EntityState.Modified;
                dbpost.SaveChanges();
                //-------------------------------------
                TempData["message"] = string.Format("{0}-Объект сохранен в базу данных", ncat.NameCategory);
                return RedirectToAction("EditPannelCategory");

            }


            
            return View(ncat);          //ошибка

        }

        //============================================================================================================

        #endregion
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    }
}