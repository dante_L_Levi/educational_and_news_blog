﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using MyBlog.WebUI.Infrastructure;
using Microsoft.AspNet.Identity;
using MyBlog.WebUI.Models;
using System.Threading.Tasks;



namespace MyBlog.WebUI.Controllers
{

    /// <summary>
    /// Класс отвечающий за создание создание/редактирования/удаления пользователей(только пользователь с ролью администратора может выполнить данные действия)
    /// </summary>
    [Authorize(Roles = "Administrators")]
    public class AdminController : Controller
    {
        /// <summary>
        /// Метод отвечающий за вывод всех пользователей(список пользователей)
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(UserManager.Users);
        }

        //===========================================================================================================
        /// <summary>
        /// Метод, отвечающий за создание новых пользователей(GET-запрос)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Метод, отвечающий за создание новых пользователей(POST-запрос)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(CreateModel model)
        {
            if(ModelState.IsValid)
            {
                AppUser user = new AppUser { UserName = model.Name, Email = model.Email };

                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if(result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    AddErrorsFromResult(result);
                }
            }
            return View(model);
        }

        //============================================================================================================


            /// <summary>
            /// Метод, отвечающий за удаление пользователей из базы данных
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            [HttpPost]
            public async Task<ActionResult> Delete(string id)
        {
            AppUser user = await UserManager.FindByIdAsync(id);

            if(user!=null)
            {
                IdentityResult result = await UserManager.DeleteAsync(user);
                if(result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Error", result.Errors);
                }
            }
            else
            {
                return View("Error", new string[] { "Пользователь не найден" });
            }

        }

        //============================================================================================================


            /// <summary>
            /// Метод, отвечающий за редактирования пользователей
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Edit(string id)
        {
            AppUser user = await UserManager.FindByIdAsync(id);
            if(user!=null)
            {
                return View(user);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        [HttpPost]
        public async Task<ActionResult> Edit(string id, string email, string password)
        {
            AppUser user = await UserManager.FindByIdAsync(id);
            if(user!=null)
            {
                user.Email = email;
                IdentityResult validEmail
                    = await UserManager.UserValidator.ValidateAsync(user);

                if(!validEmail.Succeeded)
                {
                    AddErrorsFromResult(validEmail);
                }

                IdentityResult validPass = null;
                if(password!=string.Empty)
                {
                    validPass = await UserManager.PasswordValidator.ValidateAsync(password);

                    if (validPass.Succeeded)
                    {
                        user.PasswordHash =
                            UserManager.PasswordHasher.HashPassword(password);
                    }
                    else
                    {
                        AddErrorsFromResult(validPass);
                    }
                }
                

                if ((validEmail.Succeeded && validPass == null) ||
                       (validEmail.Succeeded && password != string.Empty && validPass.Succeeded))
                {
                    IdentityResult result = await UserManager.UpdateAsync(user);
                    if(result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        AddErrorsFromResult(result);
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Пользователь не найден");
            }

            return View(user);

           


        }



        //-----------------Метод для определения ошибки---------------
        private void AddErrorsFromResult(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        //-----------------------------------------------------------

        //--------------------доступ к контексту данных-----------------------
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        //--------------------------------------------------------------------
    }
}