﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Models;
using PagedList;
using MyBlog.WebUI.Infrastructure;
using System.Data.Entity;
using System.IO;

namespace MyBlog.WebUI.Controllers
{
    /// <summary>
    /// Контроллер новостей на сайте
    /// </summary>
    public class NewsController : Controller
    {

        private INewRepository repository;              //репозиторий базы данных
        public int PageSize = 5;                        //количество элементов на странице
        //AppIdentityDbContext db = new AppIdentityDbContext();
        public NewsController(INewRepository _repository)
        {
            repository = _repository;
        }

        #region Страница с новостями сайта
        /// <summary>
        /// Метод с новостями сайта
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ViewResult NewsList(int page=1)
        {
            NewsListViewModel model = new NewsListViewModel
            {
                postsNews = repository.NewsPost
               .OrderBy(n => n.NewID)           //сортируем по id
               .Skip((page - 1) * PageSize)     //пропускаем заданное количество элементов исходя из выражения
               .Take(PageSize),                 //возращаем нужное количество элементов
                pagingInfo = new PageInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.NewsPost.Count()
                }
            };

            return View(model);
        }

        #endregion


        #region Администраторская панель редактирования
        /// <summary>
        /// Метод администрирования новостей
        /// </summary>
        /// <param name="sortOrder">Переменная отвечающая за тип сортировки: по имени(убыванию, возрастанию) или по дате(убыванию, возрастанию)</param>
        /// <param name="searchString">поисковой запрос в строке поиска</param>
        /// <param name="currentFilter"></param>
        /// <param name="page">нумерация страницы</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult AdminNewspost(string sortOrder, string searchString, string currentFilter, int? page)
        {

            ViewBag.CurrentSort = sortOrder;
            //если мы получаем пустую строку то динамический элемент  ViewBag.NameSortParm=name_des
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            //если sortOrder равен Date ViewBag.DateSortParm=date_desc иначе ViewBag.DateSortParm=Date
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            //------------------------Пагинация--------------------------
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //----------------------------------------------
            var nw = from n in repository.NewsPost
                     select n;
            //если строка поиска не равна  нулю то производим выборку из базы данных по введенным ключам
            if (!String.IsNullOrEmpty(searchString))
            {
                nw = nw.Where(n => n.Name.Contains(searchString) || n.author.Contains(searchString));


            }

            switch(sortOrder)
            {
                case "name_desc":                       //если sortOrder==name_desc сортировка по имени в порядке убывания
                    nw = nw.OrderByDescending(n => n.Name);
                    break;

                case "Date":                            //если sortOrder==Date сортировка по дате по возрастанию
                    nw = nw.OrderBy(n => n.TimeNewAdd);
                    break;

                case "date_desc":                       //если sortOrder==date_desc сортировка по дате по убыванию
                    nw = nw.OrderByDescending(n => n.TimeNewAdd);
                    break;

                default:                                //по умолчанию по имени по возрастанию
                    nw = nw.OrderBy(n => n.Name);
                    break;
                

            }

            //============================================ПАГИНАЦИЯ====================================================
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(nw.ToPagedList(pageNumber, pageSize));
            //=========================================================================================================

        }
        #endregion


        //===============================================CRUD-function================================================

        #region Создание новой новости
        /// <summary>
        /// GET-метод на создания новости
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
            public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// POST-метод обработки запроса со страницы- создание новости
        /// </summary>
        /// <param name="news">Новая модель данных</param>
        /// <param name="uploadImage">объект предаставляющий данные о новых загружаемых файлах(новом изображении)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(New news, HttpPostedFileBase uploadImage)
        {
            if(ModelState.IsValid && uploadImage!=null)
            {
                byte[] DataImg = null;
                DataImg = ConvertImage(uploadImage);
                news.ImageMain = DataImg;
                news.NameImg = SendNameImage(uploadImage);
                repository.AddNews(news);
                
            }
            TempData["message"] = string.Format("{0}-Объект сохранен в базу данных", news.Name);
            return RedirectToAction("AdminNewspost");
        }

        #endregion


        #region Вспомогательные методы
        /// <summary>
        /// Вспомогательный метод преобразования изображения
        /// </summary>
        /// <param name="uploadImage">обрабатываемый объект</param>
        /// <returns></returns>
        private byte[] ConvertImage(HttpPostedFileBase uploadImage)
        {
            byte[] imageData = null;
            //считываем переданный файл в массив байтов
            using (var binaryReader = new BinaryReader(uploadImage.InputStream))
            {
                imageData = binaryReader.ReadBytes(uploadImage.ContentLength);
            }
            return imageData;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        /// <summary>
        /// Метод получения имени из загружаемого изображения
        /// </summary>
        /// <param name="uploadImage"></param>
        /// <returns></returns>
        private string SendNameImage(HttpPostedFileBase uploadImage)
        {
            string NameImage = uploadImage.FileName; ;
            return NameImage;
        }


        //------------------------------------------------------------
        #endregion

        

            /// <summary>
            /// метод вывода описание новости
            /// </summary>
            /// <param name="id">Индификатор для выбранной новости</param>
            /// <returns></returns>
        public ActionResult Details(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }

            New newp = repository.Details(id);
            return View(newp);

        }



        #region Редактирование новости
        /// <summary>
        /// Метод GET-для редактирования новости
        /// </summary>
        /// <param name="id">Индификатор для выбранной новости</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
        public ViewResult Edit(int? id)
        {
            if(id==null)
            {
                //return HttpNotFound();
            }
            New newspos = repository.NewsPost.FirstOrDefault(p => p.NewID == id);
            return View(newspos);
        }



        /// <summary>
        /// POST-метод редактирования новости
        /// </summary>
        /// <param name="news"> Измененный объект Новости</param>
        /// <param name="image">Объект отвечающий за загружаемые файлы(изображения)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(New news, HttpPostedFileBase image=null)
        {
            if(ModelState.IsValid)
            {
                if(image!=null)
                {
                    news.mimeType = image.ContentType;
                    news.ImageMain = new byte[image.ContentLength];
                    image.InputStream.Read(news.ImageMain, 0, image.ContentLength);
                    
                }
                repository.SaveNew(news);
                TempData["message"] = string.Format("{0}-Объект сохранен в базу данных", news.Name);
                return RedirectToAction("AdminNewspost");
            }
            else
            {
                return View(news);          //ошибка
            }
           

        }

        #endregion



        /// <summary>
        /// Get-метод удаления новости
        /// </summary>
        /// <param name="id">Индификатор для выбранной новости</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult Delete(int? id)
        {
            if(id==0)
            {
                return HttpNotFound();      
            }
            else
            {
               repository.DeleteNew(id);
                return RedirectToAction("AdminNewspost");
            }
            
        }

        //============================================================================================================
    }
}