﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Models;
using PagedList;
using MyBlog.WebUI.Infrastructure;
using System.Data.Entity;


namespace MyBlog.WebUI.Controllers
{
    /// <summary>
    /// Контроллер проектов выложенных на сайте
    /// </summary>
    public class ProjectsController : Controller
    {
        private IProjectRepository repository;
        public int PageSize = 5;


        public ProjectsController(IProjectRepository _repository)
        {
            repository = _repository;
        }

        #region Список проектов
        /// <summary>
        /// Метод отвечает за вывод всех проектов из Базы данных (5 проектов на одну страницу)
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ViewResult ListProject(int page=1)
        {
            ProjectListViewModel model = new ProjectListViewModel
            {
                postProjects=repository.ProjectPost
                .OrderBy(n=>n.projectID)
                .Skip((page-1)*PageSize)
                .Take(PageSize),
                pagingInfo=new PageInfo
                {
                    CurrentPage=page,
                    ItemsPerPage=PageSize,
                    TotalItems=repository.ProjectPost.Count()
                }

            };

            return View(model);
        }

        #endregion

        //==========================Панель администрирования===============================
        #region Администраторская панель редактирования проектов
            /// <summary>
            /// Метод отображения администраторской панели редактирования проектов
            /// </summary>
            /// <param name="sortOrder">Тип сортировки</param>
            /// <param name="searchString">Поисковой запрос</param>
            /// <param name="currentFilter">Выбранный фильтр</param>
            /// <param name="page">Параметр отвечающий за пагинацию</param>
            /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        public ActionResult AdminProjectList(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            //если мы получаем пустую строку то динамический элемент  ViewBag.NameSortParm=name_des
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            //если sortOrder равен Date ViewBag.DateSortParm=date_desc иначе ViewBag.DateSortParm=Date
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            //------------------------Пагинация--------------------------
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            //----------------------------------------------
            var npr = from n in repository.ProjectPost
                     select n;
            //если строка поиска не равна  нулю то производим выборку из базы данных по введенным ключам
            if (!String.IsNullOrEmpty(searchString))
            {
                npr = npr.Where(n => n.Name.Contains(searchString) || n.Author.Contains(searchString));


            }

            switch (sortOrder)
            {
                case "name_desc":                       //если sortOrder==name_desc сортировка по имени в порядке убывания
                    npr = npr.OrderByDescending(n => n.Name);
                    break;

                case "Date":                            //если sortOrder==Date сортировка по дате по возрастанию
                    npr = npr.OrderBy(n => n.TimePublic);
                    break;

                case "date_desc":                       //если sortOrder==date_desc сортировка по дате по убыванию
                    npr = npr.OrderByDescending(n => n.TimePublic);
                    break;

                default:                                //по умолчанию по имени по возрастанию
                    npr = npr.OrderBy(n => n.Name);
                    break;


            }

            //============================================ПАГИНАЦИЯ====================================================
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(npr.ToPagedList(pageNumber, pageSize));
            //=========================================================================================================

        }
        #endregion
        //===========================CRUD function======================
        #region Создание нового проекта
        //================================Create========================
        /// <summary>
        /// Get-метод отображения страницы создания проекта
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
        public ViewResult Create()
        {
            return View(new Project());
        }
        /// <summary>
        /// POST-метод обработки запроса со страницы создания проекта
        /// </summary>
        /// <param name="project">Новый созданный объект , который сохраняется в БД</param>
        /// <param name="image">Объект отвечающий за загрузку файлов(изображений)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project project, HttpPostedFileBase image = null)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    project.mimeType = image.ContentType;
                    project.ImageMainProject = new byte[image.ContentLength];
                    image.InputStream.Read(project.ImageMainProject, 0, image.ContentLength);

                }


                repository.SaveProject(project);
                TempData["message"] = string.Format("{0} Объект сохранен", project.Name);
                return RedirectToAction("AdminProjectList");
            }
            else
            {
                //ошибка в значениях данных
                return View(project);

            }
        }

        #endregion

        #region Просмотр проекта
        //=================================Details=================================
        /// <summary>
        /// GET-метод отображения страницы с описанием проекта
        /// </summary>
        /// <param name="id">Индификатор, выбранного проекта</param>
        /// <returns></returns>
        public ActionResult Details(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            Project prd = repository.DetailsProject(id);
            return View(prd);
        }
        #endregion

        #region Редактирования проекта
        //==========================метод- Edit====================================
        /// <summary>
        /// GET-метод отображения страницы редактирования проектов
        /// </summary>
        /// <param name="projectID">Индификатор, выбранного проекта для редактирования</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrators")]
        [HttpGet]
        public ViewResult Edit(int projectID)
        {
            Project project = repository.ProjectPost.FirstOrDefault(p => p.projectID == projectID);
            return View(project);
        }

        /// <summary>
        /// POST-метод обработки запроса со страницы редактирования проектов
        /// </summary>
        /// <param name="project">Редактируемый объект</param>
        /// <param name="image">Объект,который предоставляет доступ к файлам(изображениям)</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(Project project, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    project.mimeType = image.ContentType;
                    project.ImageMainProject = new byte[image.ContentLength];
                    image.InputStream.Read(project.ImageMainProject, 0, image.ContentLength);

                }


                repository.SaveProject(project);
                TempData["message"] = string.Format("{0} Объект сохранен", project.Name);
                return RedirectToAction("AdminProjectList");
            }
            else
            {
                //ошибка в значениях данных
                return View(project);

            }
        }
        #endregion

        #region Удаление проекта
        //=======================CRUD-delete=======================================
        /// <summary>
        /// GET-метод удаление проекта
        /// </summary>
        /// <param name="projectID">Индификатор , проекта который будет удален</param>
        /// <returns></returns>
        public ActionResult Delete(int projectID)
        {
            Project project = repository.DeleteProject(projectID);

            if(project!=null)
            {
                TempData["message"] = string.Format("{0} элемент удален!!",project.Name);
            }
            return RedirectToAction("AdminProjectList");
        }
        //==========================================================
        #endregion

        #region Дополнительные методы
            /// <summary>
            /// Метод отвечающий за подгрузку изображений
            /// </summary>
            /// <param name="projectId">Индификатор выбранного проекта</param>
            /// <returns></returns>
        public FileContentResult GetImage(int projectId)
        {
            Project prod = repository.ProjectPost.FirstOrDefault(p => p.projectID == projectId);
            if (prod != null)
            {
                return File(prod.ImageMainProject, prod.mimeType);

            }
            else
            {
                return null;
            }


        }
        #endregion

    }
}