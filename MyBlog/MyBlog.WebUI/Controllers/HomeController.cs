﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Models;


namespace MyBlog.WebUI.Controllers
{
    /// <summary>
    /// Контроллер главной страницы
    /// </summary>
    public class HomeController : Controller
    {
        private INewRepository repository;      //репозиторий базы данных
        public int PageSize = 6;                //количество элементов на странице
        public HomeController(INewRepository _repository)
        {
            repository = _repository;
        }

        #region стартовая страница с новостями
        /// <summary>
        /// Метод Главной страницы сайта
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ViewResult Main(int page=1)
        {
            //======================создаем экземпляр модели для передачи в представление===========================
            NewsListViewModel model = new NewsListViewModel
            {
                postsNews=repository.NewsPost           //подключаемся к таблице новостей
                .OrderBy(n=>n.NewID)                    //сортируем во возрастанию
                .Skip((page-1)*PageSize)                //пропускаем необходимое количество страниц а все остальное возращаем
                .Take(PageSize),                        //возращаем элементы
                pagingInfo=new PageInfo
                {
                    CurrentPage=page,
                    ItemsPerPage=PageSize,
                    TotalItems=repository.NewsPost.Count()
                }
            };


            return View("Main_page",model);
        }


        /// <summary>
        /// Метод возращает выбранную новость по указанному id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult detalNews(int? id)
        {
            if(id!=null)
            {
                var postnew = repository.NewsPost.Where(a => a.NewID == id);    //по выбранному id  перенаправляем на нужную новость
                return View(postnew.ToList());
            }
            else
            {
                return HttpNotFound();
            }

        }


        #endregion

        #region Контакты
        /// <summary>
        /// Метод возращает страницу с контактами и форму обратной связи
        /// </summary>
        /// <returns></returns>
        public ViewResult contacts()
        {
            return View();
        }

        #endregion


        #region work_admin_pannel

        //===========================================================================================================
        /// <summary>
        /// Метод возращает информацию о администраторе (только администратор может осуществить данный метод)
        /// </summary>
        /// <returns></returns>

        [Authorize(Roles = "Administrators")]
        public ActionResult Index()
        {
            Dictionary<string, object> data
                = new Dictionary<string, object>();
            data.Add("Ключ", "Значение");

            return View(data);
        }
        //==========================================================================================================
        /// <summary>
        /// Метод возращает информацию о администраторе (только администратор может осуществить данный метод)
        /// </summary>
        /// <returns></returns>

        [Authorize(Roles = "Administrators")]
        public ActionResult OtherAction()
        {
            return View("Index", GetData("OtherAction"));
        }

        private Dictionary<string, object> GetData(string actionName)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();

            dict.Add("Action", actionName);
            dict.Add("Пользователь", HttpContext.User.Identity.Name);
            dict.Add("Аутентифицирован?", HttpContext.User.Identity.IsAuthenticated);
            dict.Add("Тип аутентификации", HttpContext.User.Identity.AuthenticationType);
            dict.Add("В роли Users?", HttpContext.User.IsInRole("Users"));

            return dict;
        }

        #endregion
    }
}