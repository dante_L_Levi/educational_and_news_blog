﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Infrastructure;

namespace MyBlog.WebUI.Models
{
    /// <summary>
    /// Репозиторий для хранения новостей
    /// </summary>
    public class EFNewsRepository : INewRepository
    {
        private AppIdentityDbContext context = new AppIdentityDbContext();

        public IEnumerable<New> NewsPost
        {
            get
            {
                return context.newposts;
            }
        }

        /// <summary>
        /// Добавления в базу данных новых новостей
        /// </summary>
        /// <param name="pstNws">Модель New для добавление в БД</param>
        public void AddNews(New pstNws)
        {
            context.newposts.Add(pstNws);
            SaveNew(pstNws);

        }

        /// <summary>
        /// Удаление выбранной новости из БД
        /// </summary>
        /// <param name="NewID">Индификатор выбранной новости</param>
        public void DeleteNew(int? NewID)
        {
            New npost = context.newposts.Find(NewID);
            context.newposts.Remove(npost);
            context.SaveChanges();

        }


        /// <summary>
        /// Детальное описание выбранной новости
        /// </summary>
        /// <param name="NewID"></param>
        /// <returns>Индификатор выбранной новости</returns>
        public New Details(int? NewID)
        {
            
            New npost = context.newposts.Find(NewID);

            return npost;
        }

        /// <summary>
        /// Сохранение изменений в БД
        /// </summary>
        /// <param name="product">Модель для изменений в БД</param>
        public void SaveNew(New product)
        {
           if(product.NewID==0)
            {
                context.newposts.Add(product);
            }
            else
            {
                New dbEntry = context.newposts.Find(product.NewID);
                if(dbEntry!=null)
                {
                    dbEntry.Name = product.Name;
                    dbEntry.author = product.author;
                    dbEntry.Description = product.Description;
                    dbEntry.CountView = product.CountView;
                    dbEntry.NameImg = product.NameImg;
                    dbEntry.TextNews = product.TextNews;
                    dbEntry.ImageMain = product.ImageMain;
                }
            }

            context.SaveChanges();
        }
    }
}