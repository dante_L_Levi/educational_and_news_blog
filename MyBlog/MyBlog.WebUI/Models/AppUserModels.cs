﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MyBlog.WebUI.Models
{
    /// <summary>
    /// Класс для работы с полями авторизации пользователей
    /// </summary>
    public class AppUser:IdentityUser
    {
        // Здесь будут указываться дополнительные свойства
    }
}