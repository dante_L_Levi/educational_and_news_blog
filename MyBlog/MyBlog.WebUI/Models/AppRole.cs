﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;


namespace MyBlog.WebUI.Models
{
    /// <summary>
    /// Класс по работе с ролями аунтификации
    /// </summary>
    public class AppRole:IdentityRole
    {
        public AppRole() : base() { }

        public AppRole(string name) : base(name) { }


    }
}