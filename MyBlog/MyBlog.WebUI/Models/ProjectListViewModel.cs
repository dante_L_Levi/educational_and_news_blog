﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBlog.Domain.Entities;
namespace MyBlog.WebUI.Models
{
    /// <summary>
    /// Класс для вывода данных на страницу со списком представлений
    /// </summary>
    public class ProjectListViewModel
    {
        public IEnumerable<Project> postProjects { get; set; }      //список проектов
        public PageInfo pagingInfo { get; set; }                    //модель класса пагинации

    }
}