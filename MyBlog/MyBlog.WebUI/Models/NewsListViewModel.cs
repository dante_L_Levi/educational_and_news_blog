﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBlog.Domain.Entities;

namespace MyBlog.WebUI.Models
{
    /// <summary>
    /// Модель вывода на представление 
    /// </summary>
    public class NewsListViewModel
    {
        public IEnumerable<New> postsNews { get; set; }
        public PageInfo pagingInfo { get; set; }

    }
}