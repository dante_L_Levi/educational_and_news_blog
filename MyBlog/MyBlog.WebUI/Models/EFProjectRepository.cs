﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Infrastructure;
namespace MyBlog.WebUI.Models
{
    /// <summary>
    ///  репозиторий проектов для обращения к БД
    /// </summary>
    public class EFProjectRepository : IProjectRepository
    {
        private AppIdentityDbContext context = new AppIdentityDbContext();
        public IEnumerable<Project> ProjectPost
        {
            get
            {
                return context.projectspst;
            }
        }       //список для обращения к БД

        /// <summary>
        /// Добавление нового проекта
        /// </summary>
        /// <param name="pstProject">Новая модель для добавление в БД</param>
        public void AddProject(Project pstProject)
        {
            context.projectspst.Add(pstProject);
            SaveProject(pstProject);
        }

        /// <summary>
        /// Удаление выбранного элемента из БД
        /// </summary>
        /// <param name="projectID">Индификатор выбранного проекта</param>
        /// <returns></returns>
        public Project DeleteProject(int? projectID)
        {
            Project nproject = context.projectspst.Find(projectID);
            if(nproject!=null)
            {
                context.projectspst.Remove(nproject);
                context.SaveChanges();
            }
            return nproject;

        }

        /// <summary>
        /// Детальное описание проекта
        /// </summary>
        /// <param name="projectID">Индификатор выбранного проекта</param>
        /// <returns></returns>
        public Project DetailsProject(int? projectID)
        {
            Project proj = context.projectspst.Find(projectID);
            return proj;

        }

        /// <summary>
        /// Сохранение изменений в базе данных
        /// </summary>
        /// <param name="postproject">объект для сохранение в БД</param>
        public void SaveProject(Project postproject)
        {
            if(postproject.projectID==0)
            {
                context.projectspst.Add(postproject);
            }
            else
            {
                Project dbEntry = context.projectspst.Find(postproject.projectID);
                if(dbEntry!=null)
                {
                    dbEntry.Name = postproject.Name;
                    dbEntry.Author = postproject.Author;
                    dbEntry.description = postproject.description;
                    dbEntry.NameImgProject = postproject.NameImgProject;
                    dbEntry.TextProject = postproject.TextProject;
                    dbEntry.TimePublic = postproject.TimePublic;
                    dbEntry.mimeType = postproject.mimeType;
                    dbEntry.ImageMainProject = postproject.ImageMainProject;
                }

            }

            context.SaveChanges();

        }
    }
}