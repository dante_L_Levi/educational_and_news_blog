﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyBlog.WebUI.Models
{
    /// <summary>
    /// Класс для работы с пагинацией в представлении
    /// </summary>
    public class PageInfo
    {
        public int TotalItems { get; set; }     //количество элементов
        public int ItemsPerPage { get; set; }   //количество элементов на одной странице
        public int CurrentPage { get; set; }    //выбранная страница

        public int TotalPages {
            get {
                return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);
            }
        }                                                      //количество всех страниц
    }
}