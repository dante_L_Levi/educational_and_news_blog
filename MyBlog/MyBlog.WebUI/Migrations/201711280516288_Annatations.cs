namespace MyBlog.WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Annatations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "description", c => c.String(nullable: false));
            AlterColumn("dbo.Categories", "NameCategory", c => c.String(nullable: false));
            AlterColumn("dbo.Categories", "NameImageCat", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "NamePost", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "descriptionPost", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "TextPost", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "NameImage", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "description", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "TextProject", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "Author", c => c.String(nullable: false));
            AlterColumn("dbo.Projects", "NameImgProject", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Projects", "NameImgProject", c => c.String());
            AlterColumn("dbo.Projects", "Author", c => c.String());
            AlterColumn("dbo.Projects", "TextProject", c => c.String());
            AlterColumn("dbo.Projects", "description", c => c.String());
            AlterColumn("dbo.Projects", "Name", c => c.String());
            AlterColumn("dbo.Posts", "NameImage", c => c.String());
            AlterColumn("dbo.Posts", "TextPost", c => c.String());
            AlterColumn("dbo.Posts", "Author", c => c.String());
            AlterColumn("dbo.Posts", "descriptionPost", c => c.String());
            AlterColumn("dbo.Posts", "NamePost", c => c.String());
            AlterColumn("dbo.Categories", "NameImageCat", c => c.String());
            AlterColumn("dbo.Categories", "NameCategory", c => c.String());
            AlterColumn("dbo.Categories", "description", c => c.String());
        }
    }
}
