﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using MyBlog.WebUI.Models;
using Moq;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;


namespace MyBlog.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel _kernel)
        {
            kernel = _kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            //размещение привязок 

            kernel.Bind<INewRepository>().To<EFNewsRepository>();
            kernel.Bind<IProjectRepository>().To<EFProjectRepository>();
            //------------------временное данные для проверки контейнера----------------------------------------
            //Mock<INewRepository> mock = new Mock<INewRepository>();
            //mock.Setup(m => m.NewsPost).Returns(new List<New>
            //{

            //    new New {Name="Первая статья", Description="Краткое описание первой статьи",author="Временный автор",NewID=1,TextNews="<div><p>бла балааодваоылдаоылдаоылдаоылдаовылдаоыд </p><img src='Slider_img/Deadth.jpg'</div>" },
            //    new New {Name="Вторая статья", Description="Краткое описание второй статьи",author="Временный автор",NewID=2 },
            //    new New {Name="Третья статья", Description="Краткое описание третьей статьи",author="Временный автор",NewID=3 }

            //});
            //kernel.Bind<INewRepository>().ToConstant(mock.Object);
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
           return kernel.GetAll(serviceType);
        }
    }
}