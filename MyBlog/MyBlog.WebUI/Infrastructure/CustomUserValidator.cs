﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using MyBlog.WebUI.Models;
using System.Text.RegularExpressions;
using System.Threading;
namespace MyBlog.WebUI.Infrastructure
{
    public class CustomUserValidator : IIdentityValidator<AppUser>
    {
#pragma warning disable CS1998 // В асинхронном методе отсутствуют операторы await, будет выполнен синхронный метод
        public async Task<IdentityResult> ValidateAsync(AppUser item)
#pragma warning restore CS1998 // В асинхронном методе отсутствуют операторы await, будет выполнен синхронный метод
        {
            List<string> errors = new List<string>();

            if (String.IsNullOrEmpty(item.UserName.Trim()))
                errors.Add("Вы указали пустое имя!!!!!");

            string userNamePattern = @"^[a-zA-Z0-9а-яА-Я]+$";

            if (!Regex.IsMatch(item.UserName, userNamePattern))
                errors.Add("В имени разрешается указывать буквы английского или русского языков, и цифры");

            if (!item.Email.ToLower().EndsWith("@gmail.com"))
            {
                errors.Add("Любой email-адрес, отличный от gmail.com запрещен");
                
            }


            if (errors.Count > 0)
                return  IdentityResult.Failed(errors.ToArray());

            return IdentityResult.Success;
        }
    }
}