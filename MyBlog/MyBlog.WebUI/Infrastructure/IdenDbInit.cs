﻿
using MyBlog.WebUI.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace MyBlog.WebUI.Infrastructure
{
    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<AppIdentityDbContext>
    {
        protected override void Seed(AppIdentityDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(AppIdentityDbContext context)
        {
            AppUserManager userMgr = new AppUserManager(new UserStore<AppUser>(context));
            AppRoleManager roleMgr = new AppRoleManager(new RoleStore<AppRole>(context));

            string roleName = "Administrators";
            string userName = "Admin";
            string password = "481516234250Ak32";
            string email = "dante.l.levi93@gmail.com";

            if(!roleMgr.RoleExists(roleName))
            {
                roleMgr.Create(new AppRole(roleName));
            }
            AppUser user = userMgr.FindByName(userName);
            if(user==null)
            {
                userMgr.Create(new AppUser { UserName = userName, Email = email },password);
                user = userMgr.FindByName(userName);

            }
            if(!userMgr.IsInRole(user.Id,roleName))
            {
                userMgr.AddToRole(user.Id, roleName);
            }

        }
    }
}