﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace MyBlog.WebUI.Infrastructure
{
    public class CustomPasswordValidator : IIdentityValidator<string>
    {
        public int RequiredLength { get; set; } // минимальная длина
        public CustomPasswordValidator(int length)
        {
            RequiredLength = length;
        }
        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (String.IsNullOrEmpty(item) || item.Length < RequiredLength)
            {
                return Task.FromResult(IdentityResult.Failed(
                                String.Format("Минимальная длина пароля равна {0}", RequiredLength)));
            }

            if (item.Contains("12345"))
            {
                return Task.FromResult(IdentityResult.Failed(
                                String.Format("Пароль не должен содержать последовательности чисел.")));
                
            }
            return Task.FromResult(IdentityResult.Success);


        }
    }
}