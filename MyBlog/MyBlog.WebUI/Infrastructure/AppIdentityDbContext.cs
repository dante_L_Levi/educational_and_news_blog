﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyBlog.WebUI.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyBlog.Domain.Entities;
using MyBlog.Domain.Abstruct;


namespace MyBlog.WebUI.Infrastructure
{
    /// <summary>
    /// Класс контекста базы данных
    /// </summary>
    public class AppIdentityDbContext: IdentityDbContext<AppUser>
    {

        public AppIdentityDbContext() : base("IdentityAndObjectPostNewsDb") { }

        public DbSet<New> newposts { get; set; }        //таблица новостей
        public DbSet<Post> posts { get; set; }          //таблица статей
        public DbSet<Category> categoryPosts { get; set; }  //Таблица категорий статей
        public DbSet<Project> projectspst { get; set; } //Таблица проектов


        static AppIdentityDbContext()
        {
            Database.SetInitializer<AppIdentityDbContext>(new IdentityDbInit());
        }

        public static AppIdentityDbContext Create()
        {
            return new AppIdentityDbContext();
        }
    }
}