﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Moq;
using MyBlog.Domain.Abstruct;
using MyBlog.Domain.Entities;
using MyBlog.WebUI.Controllers;
using MyBlog.WebUI.Models;
using MyBlog.WebUI.Infrastructure;
using System.Collections;
using System.Collections.Generic;
using MyBlog.WebUI.HtmlHelpers;
using System.Web.Mvc;

namespace MyBlog.UnitTests
{
    [TestClass]
    public class UnitTest1
    {

        //===========================ТЕСТЫ ПАГГИНАЦИИ СТРАНИЦ================
        [TestMethod]
        public void Can_Paginate()
        {
            //организация
            Mock<INewRepository> mock = new Mock<INewRepository>();//создаем хранилище
            mock.Setup(m => m.NewsPost).Returns(new New[]
            {
                new New {NewID=1,Name="N1" },
                new New {NewID=2,Name="N2" },
                new New {NewID=3,Name="N3" },
                new New {NewID=4,Name="N4" },
                new New {NewID=5,Name="N5" },
                new New {NewID=6,Name="N6" },
                new New {NewID=7,Name="N7" },
                new New {NewID=8,Name="N8" }
            });                             //заполнение хранилища

            HomeController controller = new HomeController(mock.Object); //создаем экземпляр используемого контроллера
            controller.PageSize = 6;                                     //количество новостей на странице
            //Действие
            NewsListViewModel result = (NewsListViewModel)controller.Main(2).Model;   //получаем данные из метода-контроллера

            //Утверждение
            New[] newArray = result.postsNews.ToArray();  //преобразуем полученные данные в массив
            Assert.IsTrue(newArray.Length == 2);//Проверяет, что указанное условие истинно(оставшиеся элементы 7, и 8 количество 2)
            Assert.AreEqual(newArray[0].Name, "N7");// Проверяет, что два указанных удвоения равны или находятся в пределах определенной точности друг от друга. Утверждение терпит неудачу, если они не находятся в пределах определенной точности друг друга.
            Assert.AreEqual(newArray[1].Name, "N8");//Проверяет, что два указанных удвоения равны или находятся в пределах определенной точности друг от друга. Утверждение терпит неудачу, если они не находятся в пределах определенной точности друг друга.


        }

        [TestMethod]
        public void Can_Generate_Page_Link()
        {
            //Организация-определение вспомогательного метода HTML
            //это необходимо для применения расширяющего метода
            HtmlHelper myHelper = null;
            //Организация-создание данных PageInfo

            PageInfo pageInfo = new PageInfo
            {
                CurrentPage=2,
                TotalItems =28,
                ItemsPerPage=10
            };

            //Организация- настройка делегата с помощью лямбда-выражения
            Func<int, string> pageUrlDelegate = i => "Page" + i;
            //Действие
            MvcHtmlString result = myHelper.PageLings(pageInfo, pageUrlDelegate);

            //Утверждение
            Assert.AreEqual(@"<a class=""btn btn-default"" href=""Page1"">1</a>" + @"<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>" + @"<a class=""btn btn-default"" href=""Page3"">3</a>", result.ToString());

        }


        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            //Организация
            Mock<INewRepository> mock = new Mock<INewRepository>();

            mock.Setup(m => m.NewsPost).Returns(new New[]
            {
                new New {NewID=1,Name="N1" },
                new New {NewID=2,Name="N2" },
                new New {NewID=3,Name="N3" },
                new New {NewID=4,Name="N4" },
                new New {NewID=5,Name="N5" }
                
            });




            //Организация
            HomeController controller = new HomeController(mock.Object);
            controller.PageSize = 3;

            //Действие
            NewsListViewModel result = (NewsListViewModel)controller.Main(2).Model;


            //Утверждение
            PageInfo pageinfo = result.pagingInfo;
            Assert.AreEqual(pageinfo.CurrentPage, 2);
            Assert.AreEqual(pageinfo.ItemsPerPage, 3);
            Assert.AreEqual(pageinfo.TotalItems, 5);
            Assert.AreEqual(pageinfo.TotalPages, 2);

        }

        //==========================================================================

        //============================ТЕСТИРОВАНИЕ ПАГИНАЦИИ НА СТРАНИЦЕ НОВОСТЕЙ========================================

        [TestMethod]
        public void Can_Paginate_NewsList()
        {
            //организация
            Mock<INewRepository> mock = new Mock<INewRepository>();//создаем хранилище
            mock.Setup(m => m.NewsPost).Returns(new New[]
            {
                new New {NewID=1,Name="N1" },
                new New {NewID=2,Name="N2" },
                new New {NewID=3,Name="N3" },
                new New {NewID=4,Name="N4" },
                new New {NewID=5,Name="N5" },
                new New {NewID=6,Name="N6" },
                new New {NewID=7,Name="N7" },
                new New {NewID=8,Name="N8" }
            });                             //заполнение хранилища

            NewsController controller = new NewsController(mock.Object); //создаем экземпляр используемого контроллера
            controller.PageSize = 6;                                     //количество новостей на странице
            //Действие
            NewsListViewModel result = (NewsListViewModel)controller.NewsList(2).Model;   //получаем данные из метода-контроллера

            //Утверждение
            New[] newArray = result.postsNews.ToArray();  //преобразуем полученные данные в массив
            Assert.IsTrue(newArray.Length == 2);//Проверяет, что указанное условие истинно(оставшиеся элементы 7, и 8 количество 2)
            Assert.AreEqual(newArray[0].Name, "N7");// Проверяет, что два указанных удвоения равны или находятся в пределах определенной точности друг от друга. Утверждение терпит неудачу, если они не находятся в пределах определенной точности друг друга.
            Assert.AreEqual(newArray[1].Name, "N8");//Проверяет, что два указанных удвоения равны или находятся в пределах определенной точности друг от друга. Утверждение терпит неудачу, если они не находятся в пределах определенной точности друг друга.


        }




        //========================================================================================

       


        [TestMethod]
        public void Can_Send_Pagination_View_Model_NewsList()
        {
            //Организация
            Mock<INewRepository> mock = new Mock<INewRepository>();

            mock.Setup(m => m.NewsPost).Returns(new New[]
            {
                new New {NewID=1,Name="N1" },
                new New {NewID=2,Name="N2" },
                new New {NewID=3,Name="N3" },
                new New {NewID=4,Name="N4" },
                new New {NewID=5,Name="N5" }

            });




            //Организация
            NewsController controller = new NewsController(mock.Object);
            controller.PageSize = 3;

            //Действие
            NewsListViewModel result = (NewsListViewModel)controller.NewsList(2).Model;


            //Утверждение
            PageInfo pageinfo = result.pagingInfo;
            Assert.AreEqual(pageinfo.CurrentPage, 2);
            Assert.AreEqual(pageinfo.ItemsPerPage, 3);
            Assert.AreEqual(pageinfo.TotalItems, 5);
            Assert.AreEqual(pageinfo.TotalPages, 2);

        }

        //===============================================================================================================



        [TestMethod]
        public void Can_edit_News()
        {
            Mock<INewRepository> mock = new Mock<INewRepository>();
            mock.Setup(m => m.NewsPost).Returns(new List<New>
            {
                new New {NewID=1,Name="N1" },
                new New {NewID=2,Name="N2" },
                new New {NewID=3,Name="N3" },
                new New {NewID=4,Name="N4" },
                new New {NewID=5,Name="N5" }
            });

            //организация -создание контроллера
            NewsController controller = new NewsController(mock.Object);

            New newsp1 = controller.Edit(1).ViewData.Model as New;
            New newsp2 = controller.Edit(2).ViewData.Model as New;
            New newsp3 = controller.Edit(3).ViewData.Model as New;


            // Assert
            Assert.AreEqual(1, newsp1.NewID);
            Assert.AreEqual(2, newsp2.NewID);
            Assert.AreEqual(3, newsp3.NewID);


        }

        [TestMethod]
        public void Cannot_Edit_Nonexistent_Game()
        {
            // Организация - создание имитированного хранилища данных
            Mock<INewRepository> mock = new Mock<INewRepository>();

            mock.Setup(m => m.NewsPost).Returns(new List<New>
            {

                new New {NewID=1,Name="N1" },
                new New {NewID=2,Name="N2" },
                new New {NewID=3,Name="N3" },
                new New {NewID=4,Name="N4" },
                new New {NewID=5,Name="N5" }

            });

            //организация -создание контроллера
            NewsController controller = new NewsController(mock.Object);

            New newsp1 = controller.Edit(6).ViewData.Model as New;

        }
    }
}
